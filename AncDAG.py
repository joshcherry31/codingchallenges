from typing import List

class CorrectSolution:
    def getAncestors(self, n: int, edges: List[List[int]]) -> List[List[int]]:
        DirectChild = [[] for _ in range(n)]
        sol = [[] for _ in range(n)]
        for l in edges:
            DirectChild[l[0]].append(l[1])
        print(DirectChild)
        for i in range(n):
            self.DFS(i, i, sol, DirectChild)
        return sol
    
    def DFS(self, x: int, curr: int, sol: List[List[int]], DC: List[List[int]]) -> None:
        for ch in DC[curr]:
            if not sol[ch] or sol[ch][-1] != x:
                sol[ch].append(x)
                self.DFS(x, ch, sol, DC)
            
class MySolution:
    def getAncestors(self, n: int, edges: List[List[int]]) -> List[List[int]]:
        sol = [[] for _ in range(n)]
        for l in edges:
            if l[0] not in sol[l[1]]:
                sol[l[1]].append(l[0])
            print(l)
            if len(sol[l[0]]) != 0:
                for i in sol[l[0]]:
                    if i not in sol[l[1]]:
                        sol[l[1]].append(i)
        for l in sol:
            l.sort()
        return sol

def main():
    edges = [[0,3],[5,0],[2,3],[4,3],[5,3],[1,3],[2,5],[0,1],[4,5],[4,2],[4,0],[2,1],[5,1]]
    n = 6
    p1 = CorrectSolution()
    sol = p1.getAncestors(n, edges)
    print(sol)

if __name__ == "__main__":
    main()   