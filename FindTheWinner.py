class FastSolution:
    def findTheWinner(self, n: int, k: int) -> int:
        winner = 0 
        for i in range(1, n + 1):
            winner = (winner + k) % i
        return winner + 1  

class Solution:
    def findTheWinner(self, n: int, k: int) -> int:
        if k == 1:
            return n
        numList = []
        count = 1
        x = 0
        for i in range(n):
            numList.append(i+1)
        while len(numList) != 1:
            count += 1
            x += 1
            if x >= len(numList):
                x = 0
            if count == k:
                count = 1
                numList.pop(x)
            if x == len(numList):
                x = 0
        return numList[0]
def main():
    n = 6
    k = 5
    p1 = FastSolution()
    sol = p1.findTheWinner(n,k)
    print(sol)

if __name__ == "__main__":
    main()   