# You are given the root of a binary tree and an integer distance. A pair of two different leaf nodes of a binary tree is said 
# to be good if the length of the shortest path between them is less than or equal to distance.
# Return the number of good leaf node pairs in the tree.
# Input: root = [1,2,3,null,4], distance = 3
# Output: 1
# Explanation: The leaf nodes of the tree are 3 and 4 and the length of the shortest path between them is 3. This is the only good pair.

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
def build_tree(values):
    if not values:
        return None

    root = TreeNode(values[0])
    queue = [root]
    i = 1

    while i < len(values):
        current = queue.pop(0)

        if values[i] is not None:
            current.left = TreeNode(values[i])
            queue.append(current.left)
        i += 1

        if i < len(values) and values[i] is not None:
            current.right = TreeNode(values[i])
            queue.append(current.right)
        i += 1

    return root
class Solution:
    def countPairs(self, root: TreeNode, distance: int) -> int:
        self.sol = 0
        def DFS(root):
            if not root:
                return []
            if not root.left and not root.right:
                return [1]
            left = DFS(root.left)
            right = DFS(root.right)
            for i in left:
                for j in right:
                    if i+j <= distance:
                        self.sol += 1
            return [i+1 for i in left+right if i+1 < distance]
        DFS(root)
        return self.sol
          
def main():
    values  = [1,2,3,None,4]
    root = build_tree(values)
    p1 = Solution()
    sol = p1.countPairs(root, 3)
    print(sol)
    
if __name__ == "__main__":
    main()   