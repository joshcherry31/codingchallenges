# A distinct string is a string that is present only once in an array.
# Given an array of strings arr, and an integer k, return the kth distinct string present in arr. If there are fewer than k distinct strings, return an empty string "".
# Note that the strings are considered in the order in which they appear in the array.
# Example 1:
# Input: arr = ["d","b","c","b","c","a"], k = 2
# Output: "a"
# Explanation:
# The only distinct strings in arr are "d" and "a".
# "d" appears 1st, so it is the 1st distinct string.
# "a" appears 2nd, so it is the 2nd distinct string.
# Since k == 2, "a" is returned. 
from typing import List
from collections import Counter

class BestSolution:
    def kthDistinct(self, arr: List[str], k: int) -> str:
        counter = Counter(arr)
        for v in arr:
            if counter[v] == 1:
                k -= 1
                if k == 0:
                    return v
        return ''

class Solution:
    def kthDistinct(self, arr: List[str], k: int) -> str:
        NonDis = []
        testDict = []
        for char in arr:
            if char not in testDict and char not in NonDis:
                testDict.append(char)
            elif char not in NonDis:
                NonDis.append(char)
                testDict.remove(char)
        if k > len(testDict):
            return ""
        return testDict[k-1]
          
def main():
    arr = ["c","exjk","nbmg","kgnas","s","oydx","ghpao","c","r","ohdm","fq","ashgg","mm","cc","mymy","w","t","neb","grjdb","cukk","ujyhn","dq","hhuo","qu","seslw","ybulz","iug","rs","kyfu","krz","nw","txnn","r","zpuao","sh","rfc","c","hgr","jfia","egm","gmuuv","gh","x","nfvgv","ibo","al","wn","o","dyu","zgkk","gdzrf","m","ui","xwsj","zeld","muowr","d","xgiu","yfu"]
    k = 19
    p1 = BestSolution()
    sol = p1.kthDistinct(arr, k)
    print(sol)
    
if __name__ == "__main__":
    main()   