import heapq
from typing import List

class Solution:
    def maxTwoEvents(self, events: List[List[int]]) -> int:
        count = 0
        max = 0
        for event in events:
            if event[2] > max:
                max = event[2]
        for event_one in events:
            first = 0
            for event_two in events:
                if first == count:
                    print(first)
                    first += 1
                    continue
                first += 1
                print("Event 1 - " + str(event_one))
                print("Event 2 - " + str(event_two))
                print((event_two[0] - event_one[1]))
                if (event_two[0] - event_one[1]) > 1:
                    if (event_two[2] + event_one[2]) > max:
                        max = event_two[2] + event_one[2]
            count += 1
        return max
    
class CorrectSolution:
    def maxTwoEvents(self, events: List[List[int]]) -> int:
        events.sort()
        heap = []
        res2,res1 = 0,0
        for s,e,p in events:
            while heap and heap[0][0]<s:
                res1 = max(res1,heapq.heappop(heap)[1])
            
            res2 = max(res2,res1+p)
            heapq.heappush(heap,(e,p))
        
        return res2
     
def main():
    events = [[10,83,53],[63,87,45],[97,100,32],[51,61,16]]
    p1 = Solution()
    sol = p1.maxTwoEvents(events)
    print(sol)
    
if __name__ == "__main__":
    main()   