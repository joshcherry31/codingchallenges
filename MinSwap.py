# A swap is defined as taking two distinct positions in an array and swapping the values in them.
# A circular array is defined as an array where we consider the first element and the last element to be adjacent.
# Given a binary circular array nums, return the minimum number of swaps required to group all 1's present in the array together at any location.
# Example 1:
# Input: nums = [0,1,0,1,1,0,0]
# Output: 1
# Explanation: Here are a few of the ways to group all the 1's together:
# [0,0,1,1,1,0,0] using 1 swap.
# [0,1,1,1,0,0,0] using 1 swap.
# [1,1,0,0,0,0,1] using 2 swaps (using the circular property of the array).
# There is no way to group all 1's together with 0 swaps.
# Thus, the minimum number of swaps required is 1.
from typing import List

class Solution:
    def minSwaps(self, nums: List[int]) -> int:
        window = 0 
        count = 0
        max_count = 0
        arr_length = len(nums)
        for val in nums:
            if val == 1:
                window += 1    
        
        # Initialize the count of 1s in the first window
        for i in range(window):
            if nums[i] == 1:
                count += 1
                max_count += 1
                
        for i in range(window, arr_length + window):
            print("i - " + str(i))
            print(i % arr_length)
            count += nums[i % arr_length]
            print((i - window + arr_length) % arr_length)
            count -= nums[(i - window + arr_length) % arr_length]
            max_count = max(max_count, count)
            print("max count - " + str(max_count))
        return window - max_count
        
        
    
def main():
    nums = [1,1,0,0,1]
    p1 = Solution()
    sol = p1.minSwaps(nums)
    print(sol)
    
if __name__ == "__main__":
    main()   