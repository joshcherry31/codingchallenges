class FastSolution:
    def passThePillow(self, n: int, time: int) -> int:
        chunks = time // (n - 1)
        return (time % (n - 1) + 1) if chunks % 2 == 0 else (n - time % (n - 1))
        

class Solution:
    def passThePillow(self, n: int, time: int) -> int:
        person = 1
        reverse = False
        for x in range(time):
            if person == n:
                reverse = True
            if person == 1:
                reverse = False
            if reverse:
                person -= 1
            else:
                person += 1
        return person
        

def main():
    n = 18
    time = 38
    p1 = Solution()
    sol = p1.passThePillow(n,time)
    print(sol)

if __name__ == "__main__":
    main()   