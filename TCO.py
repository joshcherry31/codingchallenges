from typing import List

class FasterSolution:
    def threeConsecutiveOdds(self, arr: List[int]) -> bool:
        n = len(arr)
        for i in range(n-2):
            if arr[i] % 2 != 0 and arr[i+1] % 2 != 0 and arr[i+2] % 2!= 0:
                return True
        return False

class SolutionOne:
    def threeConsecutiveOdds(self, arr: List[int]) -> bool:
        count = 0
        for i in arr:
            if i % 2 != 0:
                count += 1
            else:
                count = 0
            if count == 3:
                return True
        return False

def main():
    arr = [1,2,34,3,4,5,7,23,12]
    arr2 = [2,6,4,1]
    p1 = FasterSolution()
    sol = p1.threeConsecutiveOdds(arr)
    print(sol)
    sol2 = p1.threeConsecutiveOdds(arr2)
    print(sol2)

if __name__ == "__main__":
    main()   