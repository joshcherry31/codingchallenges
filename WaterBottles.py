import math

class fastSolution:
    def numWaterBottles(self, numBottles: int, numExchange: int) -> int:
        return numBottles + (numBottles-1)//(numExchange-1)

class Solution:
    def numWaterBottles(self, numBottles: int, numExchange: int) -> int:
        BottlesToDrink = numBottles
        start = numBottles
        leftovers = 0
        while start >= numExchange:
            BottlesToDrink = BottlesToDrink + math.floor(start/numExchange)
            leftovers = start % numExchange
            start = math.floor(start/numExchange) + leftovers
        return BottlesToDrink

def main():
    numBottles = 15
    numExchange = 4
    p1 = fastSolution()
    sol = p1.numWaterBottles(numBottles,numExchange)
    print(sol)

if __name__ == "__main__":
    main()   