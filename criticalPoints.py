from typing import List
from typing import Optional

class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution(object):
    def build_list(self, nums: List[int]) -> Optional[ListNode]:
        head = ListNode(nums[0])
        current = head
        
        for value in nums[1:]:
            current.next = ListNode(value)
            current = current.next
        return head
    
    def nodesBetweenCriticalPoints(self, head):
        """
        :type head: Optional[ListNode]
        :rtype: List[int]
        """
        critical_points = []
        current = head.next
        prev = head
        count = 1
        while current.next:
            if current.val > prev.val and current.val > current.next.val:
                critical_points.append(count)
                    
            if current.val < prev.val and current.val < current.next.val:
                critical_points.append(count)
            prev = current
            current = current.next
            count += 1
        if len(critical_points) > 1:
            sol = [100000,0]
            for i in range(len(critical_points) - 1):
                if critical_points[i+1] - critical_points[i] < sol[0]:
                    sol[0] = critical_points[i+1] - critical_points[i]
            sol[1] = critical_points[-1] - critical_points[0]
            return sol
        else:
            return [-1,-1]
    
def main():
    head = [6,8,4,1,9,6,6,10,6]
    p1 = Solution()
    head = p1.build_list(head)
    sol = p1.nodesBetweenCriticalPoints(head)
    print(sol)
    
if __name__ == "__main__":
    main()   