from typing import List

class Solution:
    def intersect(self, nums1: List[int], nums2: List[int]) -> List[int]:
        dict1 = {}
        sol = []
        for i in nums1:
            if i not in dict1:
                dict1[i] = 1
            else:
                dict1[i] += 1
        for i in nums2:
            if i in dict1 and dict1[i] > 0:
                sol.append(i)
                dict1[i] -= 1
        return sol
            
        
def main():
    nums1 = [1,2,2,1]
    nums2 = [2,2]
    p1 = Solution()
    sol = p1.intersect(nums1, nums2)
    print(sol)

if __name__ == "__main__":
    main()   