from typing import Optional
from typing import List

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def build_list(self, nums: List[int]) -> Optional[ListNode]:
        head = ListNode(nums[0])
        current = head
        
        for value in nums[1:]:
            current.next = ListNode(value)
            current = current.next
        return head
    
    def mergeNodes(self, head: Optional[ListNode]) -> Optional[ListNode]:
        newList = ListNode(0)
        headNewList = newList
        current = head.next
        sum = 0
        while current is not None:
            if current.val == 0 and sum != 0:
                headNewList.next = ListNode(sum)
                headNewList = headNewList.next
                sum = 0
            else:
                sum += current.val
            current = current.next
        return newList.next

def main():
    p1 = Solution()
    head = p1.build_list([0,3,1,0,4,5,2,0])
    sol = p1.mergeNodes(head)
    while sol != None:
        print(sol.val)
        sol = sol.next

if __name__ == "__main__":
    main()   