from typing import List

class CorrectSolution:
    def minDifference(self, nums: List[int]) -> int:
        if len(nums) <= 4:
            return 0
        nums.sort()
        ans = nums[-1] - nums[0]
        for i in range(4):
            ans = min(ans, nums[-(4 - i)] - nums[i])
        return ans

class IncorrectSolution:
    def minDifference(self, nums: List[int]) -> int:
        largest = [0,0]
        smallest = [0,0]
        sol = 0
        min = 0
        max = 0
        if len(nums) <= 4:
            return 0
        for _ in range(3):
            for i in range(len(nums)):
                if nums[i] >= largest[0]:
                    largest[0] = nums[i]
                    largest[1] = i
                if nums[i] <= smallest[0]:
                    smallest[0] = nums[i]
            nums[largest[1]] = smallest[0]
            largest = [0,0]
        for i in nums:
            if i <= min:
                min = i
            if i >= max:
                max = i
            sol = max - min
        return sol

def main():
    nums = [6,6,0,1,1,4,6]
    p1 = CorrectSolution()
    sol = p1.minDifference(nums)
    print(sol)

if __name__ == "__main__":
    main()   