from typing import List

class Solution:
    def minOperations(self, logs: List[str]) -> int:
        steps = 0
        for path in logs:
            if path == "../" and steps == 0:
                continue
            if path == "../" and steps != 0:
                steps = steps - 1
                continue
            if path == "./":
                continue
            steps += 1
        return steps
    
    
def main():
    logs = ["d1/","d2/","./","d3/","../","d31/"]
    p1 = Solution()
    sol = p1.minOperations(logs)
    print(sol)
    
if __name__ == "__main__":
    main()   