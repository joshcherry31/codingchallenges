# You are given an undirected weighted graph of n nodes (0-indexed), represented by an edge 
# list where edges[i] = [a, b] is an undirected edge connecting the nodes a and b with a 
# probability of success of traversing that edge succProb[i]. Given two nodes start and end, 
# find the path with the maximum probability of success to go from start to end and return 
# its success probability. If there is no path from start to end, return 0. Your answer will 
# be accepted if it differs from the correct answer by at most 1e-5.

# Input: n = 3, edges = [[0,1],[1,2],[0,2]], succProb = [0.5,0.5,0.2], start = 0, end = 2
# Output: 0.25000
# Explanation: There are two paths from start to end, one having a probability of success = 0.2 and the other has 0.5 * 0.5 = 0.25.


from typing import List

class Solution:
    def maxProbability(self, n: int, edges: List[List[int]], succProb: List[float], start_node: int, end_node: int) -> float:
        dist = [0] * n
        dist[start_node] = 1
        
        for _ in range(n - 1):
            updated = False
            for i, (u, v) in enumerate(edges):
                if dist[u] * succProb[i] > dist[v]:
                    dist[v] = dist[u] * succProb[i]
                    updated = True
                if dist[v] * succProb[i] > dist[u]:
                    dist[u] = dist[v] * succProb[i]
                    updated = True
            if not updated:
                break
        
        return dist[end_node]
        

def main():
    n = 3
    edges = [[0,1],[1,2],[0,2]]
    succProb = [0.5,0.5,0.2] 
    start = 0
    end = 2
    p1 = Solution()
    sol = p1.maxProbability(n, edges, succProb, start, end)
    print(sol)

if __name__ == "__main__":
    main()   